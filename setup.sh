#!/bin/bash

echo "Updating Apt"

sudo apt-get -y update && \
sudo apt-get -y upgrade && \
    
# Install Dependencies
sudo apt-get -y install \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg-agent \
    net-tools \
    npm \
    python \
    python3-pip \
    software-properties-common && \

# Update NPM to latest version
sudo npm install npm@latest -g && \

# Install global NPM requirements
sudo npm install -g nodemon && \

# Install Docker
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | yes '' | sudo apt-key add - && \
sudo apt-key fingerprint 0EBFCD88 && \
sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable" && \
sudo apt-get -y update && \
sudo apt-get -y install docker-ce \
    docker-ce-cli \
    containerd.io && \
    
# Install Docker Compose
# sudo curl -L "https://github.com/docker/compose/releases/download/1.25.4/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose && \
# sudo chmod +x /usr/local/bin/docker-compose
pip install docker-compose && \

# Install Terraform
curl -fsSL https://apt.releases.hashicorp.com/gpg | sudo apt-key add - && \
sudo apt-add-repository -y "deb [arch=$(dpkg --print-architecture)] https://apt.releases.hashicorp.com $(lsb_release -cs) main"  && \
sudo apt-get -y update && sudo apt-get -y install terraform  && \

# Make code folder

mkdir /code
