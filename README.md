# Quickstart Ubuntu Web-Development Environment

## Purpose

- Provides a way to repeatedly install ubuntu for local or remote development on a clean install
- Installs the following services:
    - Docker
    - NodeJS

## Prerequisites

- Install a copy of Ubuntu Server on a remote machine (e.g. AWS) or locally on a hypervisory (e.g. Hyper-V)

## Installation Instructions

1. Install your copy of Ubuntu server onto either Hyper-V, VMWare or VirtualBox locally and complete the installation steps until it reaches the command prompt where you are able to login
2. Download MobaXTerm - https://mobaxterm.mobatek.net/download-home-edition.html
3. Install SSH Server onto your remote machine (Hyper-V, VMWare or VirtualBox) by entering the following code into the Hypervisor window...
```
sudo apt-get -y install ssh-server
```
4. Find the local network IP address of the remote server and note it down
5. Then on the local machine, load MobaXterm and start a local terminal window
6. Copy and paste this into the command line....
```
rm -f ~/.ssh/id_rsa && rm -f ~/setup.* && \
ssh-keygen -b 4096 -f ~/.ssh/id_rsa -N "" && \
cat ~/.ssh/id_rsa.pub && \
read -p 'Copy the above SSH key into Gitlab.com and press ENTER' && \
read -p 'What is the IP address of the remote machine? :' IPADDRESS && \
ssh-copy-id matt@"$IPADDRESS" && \
wget https://gitlab.com/mattreeves/uberdev-setup/-/raw/master/setup.sh && \
scp ~/setup.sh matt@"$IPADDRESS":/tmp/ && ssh -t matt@"$IPADDRESS" "sudo -s bash /tmp/setup.sh"
```

7. Sit back and relax as the script installs what is required...